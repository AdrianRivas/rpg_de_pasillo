﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x,20,transform.position.z),speed*Time.deltaTime); 
        if (transform.position.y >= 18.33f||Input.GetKeyDown(KeyCode.Space)==true)
        {
            SceneManager.LoadScene(2);
        }
    }
}
