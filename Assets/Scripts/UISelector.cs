﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISelector : MonoBehaviour
{
    public GameObject sc1, sc2, sc3,instSound;
    public AudioClip selectAudio;
    public bool playsoundS = false;
    public AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Horizontal")>0&& playsoundS==false)
        {
            playsoundS = true;
            PlaySoundSelect();
        }
    }
    void PlaySoundSelect()
    {
        Destroy(instSound= Instantiate(audioSource.gameObject), 5);
        StartCoroutine(SoundRoutine());
    }
    IEnumerator SoundRoutine()
    {
        yield return new WaitForSeconds(2);
        playsoundS = false;
    }
}
