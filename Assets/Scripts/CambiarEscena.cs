﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarEscena : MonoBehaviour
{
    public int Escena,EscenaGame,exit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadGameScene()
    {
        SceneManager.LoadScene(EscenaGame);
    }
    private void OnMouseDown()
    {
        if (Escena!=exit)
        {
            if (Escena ==EscenaGame)
            {

            }
            else
            {
                SceneManager.LoadScene(Escena);
            }
        }
        else if(Escena ==exit)
        {
            Application.Quit();
        }
    }
}
